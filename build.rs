extern crate xml;

use xml::{EventReader, ParserConfig};
use xml::attribute::OwnedAttribute;
use xml::reader::XmlEvent::*;

use std::fs::File;
use std::io::Write;
use std::process::Command;

fn main() {
    let interfaces = load_protocol();
 
    // Write server library
    
    let mut out = File::create("src/lib.rs").unwrap();
    
    out.write(HEADER.as_bytes()).unwrap();
    
    write_interface_structs_and_enums(&mut out, &interfaces);
    write_interfaces_enum(&mut out, &interfaces);
    
    // This is deprecated in my mind, so not (yet) implementing for Event.
    write_request_uses(&mut out, &interfaces);
    
    write_request_enum(&mut out, &interfaces);
    write_event_enum(&mut out, &interfaces);
    
    out.write(DATA_TYPES.as_bytes()).unwrap();
    out.write(CONNECTION.as_bytes()).unwrap();
    
    out.write(PRIVATE.as_bytes()).unwrap();
    
    write_connection_get_request(&mut out, &interfaces);
    write_connection_get_event(&mut out, &interfaces);
    
    write_message_iter(&mut out, "Events", "Event", "event");
    write_message_iter(&mut out, "Requests", "Request", "request");
    
    println!("fmt");
    Command::new("cargo")
        .arg("fmt")
        .output()
        .map(|x| println!("{:?}", x))
        .unwrap_or_else(|_| println!("Failed to execute"));
}

// Loading code

fn u32_from_str(s: &str) -> Option<u32> {
    use std::str::FromStr;
    if s.len() > 2 && &s[..2] == "0x" {
        u32::from_str_radix(&s[2..], 16).ok()
    }
    else {
        u32::from_str(s).ok()
    }
}

#[derive(Debug)]
struct Arg {
    name: String,
    typ: String,
    interface: Option<String>,
    interface_arg: Option<String>,
    version_arg: Option<String>,
    summary: Option<String>,
    enum_: Option<String>,
    allow_null: bool,
}

/// Either a Request or an Event.
#[derive(Debug)]
struct Message {
    name: String,
    summary: String,
    description: String,
    args: Vec<Arg>
}

#[derive(Debug)]
struct Entry {
    name: String,
    value: u32,
    summary: Option<String>
}

#[derive(Debug)]
struct Enum {
    name: String,
    summary: String,
    description: String,
    entrys: Vec<Entry>,
    bitfield: bool
}


#[derive(Debug)]
struct Interface {
    name: String,
    version: u32,
    summary: String,
    description: String,
    requests: Vec<Message>,
    events: Vec<Message>,
    enums: Vec<Enum>
}

fn parse_description(inp: &mut EventReader<File>) -> Result<(String, String), xml::reader::XmlEvent> {
    // Get summary
    let elem = inp.next().unwrap();
    
    let attrs = match elem {
        StartElement{name: ref elem_name, ref attributes, ..} 
            if elem_name.local_name == "description" => attributes,
        _ => return Err(elem)
    };
    assert!(attrs.len() == 1);
    assert!(attrs[0].name.local_name == "summary");
    let summary = attrs[0].value.clone();
    
    // Get description
    let elem = inp.next().unwrap();
    let description = match elem {
        Characters(text) => text,
        EndElement{..} => return Ok((summary, "".into())),
        _ => panic!("ERROR: Unexpected element: {:?}", elem),
    };
    
    // Remove extraneous spaces.
    let description = description.replace("    ", "");
    
    // Throw out description end
    inp.next().unwrap();
    
    Ok((summary, description))
}

fn get_message(attributes: &[OwnedAttribute], inp: &mut EventReader<File>) -> Message {
    // destructors have type // assert!(attributes.len() == 1);
    assert!(attributes[0].name.local_name == "name");
    let name = attributes[0].value.clone();
    
    let (summary, description) = parse_description(inp).unwrap();
    let mut args = vec!();
    
    loop {
        let elem = inp.next().unwrap();
        match elem {
            StartElement{ name, attributes, .. } => {
                assert!(name.local_name == "arg");
                
                let mut name = None;
                let mut typ = None;
                let mut interface = None;
                let mut interface_arg = None;
                let mut version_arg = None;
                let mut summary = None;
                let mut enum_ = None;
                let mut allow_null = false;
                
                for attr in attributes {
                    if attr.name.local_name == "name" {
                        name = Some(attr.value);
                    }
                    else if attr.name.local_name == "type" {
                        typ = Some(attr.value);
                    }
                    else if attr.name.local_name == "interface" {
                        interface = Some(attr.value);
                    }
                    else if attr.name.local_name == "summary" {
                        summary = Some(attr.value);
                    }
                    else if attr.name.local_name == "enum" {
                        enum_ = Some(attr.value);
                    }
                    else if attr.name.local_name == "allow-null" {
                        assert!(attr.value == "true");
                        allow_null = true;
                    }
                    else if attr.name.local_name == "interface-arg" {
                        interface_arg = Some(attr.value);
                    }
                    else if attr.name.local_name == "version-arg" {
                        version_arg = Some(attr.value);
                    }
                    else { println!("WARN: Unexpected attribute: {:?}", attr) }
                }
                
                let name = name.unwrap();
                let typ = typ.unwrap();
                
                // Throwaway end tag
                inp.next().unwrap();
                
                args.push(Arg{ 
                    name: name, 
                    typ: typ, 
                    interface: interface, 
                    summary: summary, 
                    enum_: enum_, 
                    allow_null: allow_null, 
                    interface_arg: interface_arg, 
                    version_arg: version_arg 
                });
            }
            EndElement{..} => break,
            _ => panic!("Unexpected element!"),
        }
    }
    
    Message {
        name: name,
        summary: summary,
        description: description,
        args: args
    }
}

fn get_interface(inp: &mut EventReader<File>, attributes: &[OwnedAttribute]) -> Interface {
    // Get name and version
    let mut name = None;
    let mut version = None;
    
    for attr in attributes {
        if attr.name.local_name == "name" {
            name = Some(attr.value.clone());
        }
        else if attr.name.local_name == "version" {
            version = u32_from_str(&attr.value)
        }
    }
    
    let name = name.unwrap();
    let version = version.unwrap();
    
    let (summary, description) = parse_description(inp).unwrap();
    
    // Done with the basic information

    
    // Get Requests Events and Enums in no particular order
    let mut requests = vec!();
    let mut events = vec!();
    let mut enums = vec!();
    
    loop {
        let elem = inp.next().unwrap();
        match elem {
            StartElement{ref name, ref attributes, ..} if name.local_name == "request" => {
                requests.push( get_message(attributes, inp) );
            }
            StartElement{ref name, ref attributes, ..} if name.local_name == "event" => {
                events.push( get_message(attributes, inp) );
            }
            StartElement{ref name, ref attributes, ..} if name.local_name == "enum" => {
                let mut name = None;
                let mut bitfield = false;
                
                for attr in attributes {
                    match attr.name.local_name.as_str() {
                        "name" => name = Some(attr.value.clone()),
                        "bitfield" => {
                            assert!(attr.value == "true");
                            bitfield = true
                        },
                        _ => println!("WARN: Unexpected attr in enum: {:?}", attr),
                    }
                }
                
                let name = name.unwrap();
                
                let (mut elem, summary, description) = match parse_description(inp) {
                    Ok((s, d)) => (inp.next().unwrap(), s, d),
                    Err(e) => (e, "".into(), "".into())
                };
                let mut entrys = vec!();
                
                
                while let StartElement{attributes, ..} = elem {
                    let mut name = None;
                    let mut value = None;
                    let mut summary = None;
                    
                    for attr in attributes {
                        if attr.name.local_name == "name" {
                            name = Some(attr.value);
                        }
                        else if attr.name.local_name == "value" {
                            value = u32_from_str(&attr.value);
                        }
                        else if attr.name.local_name == "summary" {
                            summary = Some(attr.value);
                        }
                        else if attr.name.local_name == "bitfield" {
                            assert!(attr.value == "true");
                            bitfield = true;
                        }
                        else {
                            panic!("Unexpected attribute");
                        }
                    }
                    
                    let name = name.unwrap();
                    let value = value.unwrap();
                    let summary = summary;
                    
                    entrys.push(Entry{ name: name, value: value, summary:summary });
                    
                    // Kill end tag
                    inp.next().unwrap();
                    
                    
                    elem = inp.next().unwrap();
                }
                
                enums.push(Enum{name: name, summary: summary, description: description, entrys: entrys, bitfield: bitfield});
            },
            EndElement{..} => break,
            _ => panic!("Unexpected tag: {:?}", elem),
        }
    }
    
    Interface {
        name: name,
        version: version,
        summary: summary,
        description: description,
        requests: requests,
        events: events,
        enums: enums
    }
}

fn load_protocol() -> Vec<Interface> {
    // This would be a static except it's not Copy!
    // Bring back Copy By Default!
    let config: ParserConfig = ParserConfig {
        trim_whitespace: true,
        whitespace_to_characters: false,
        cdata_to_characters: false,
        ignore_comments: true,
        coalesce_characters: true,
    };
    // Load file into a Vec<Interface>
    let inp = File::open("protocol/wayland.xml").unwrap();
    let mut inp = EventReader::new_with_config(inp, config);
    
    let mut contents = vec!();
    
    loop {
        let elem = inp.next();
        match elem.unwrap() {
            StartElement{name: ref elem_name, ref attributes, ..} 
                if elem_name.local_name == "interface" => contents.push(get_interface(&mut inp, attributes)),
            EndDocument => break,
            _ => (),
        }
    }  
    
    contents
}

// Printing code below here

fn wl_to_rust_name(name: &str) -> String {
    let mut rname = name.chars().scan('_', |prev, cur| {
        let ret = Some( if '_' == *prev { cur.to_uppercase().next().unwrap() }
                  else { cur } );
        *prev = cur;
        ret
    }).filter(|x| *x != '_').collect::<String>();
    
    if rname.chars().next().unwrap().is_numeric() {
        rname.insert(0, '_');
    }
    
    rname
}

fn wl_to_rust_type(interface: &Interface, arg: &Arg) -> String {
    if let Some(ref enum_) = arg.enum_ {
        let mut ret = "::interfaces::".to_owned();
        ret.push_str(&interface.name[3..]);
        ret.push_str("::");
        ret.push_str(enum_);
        return ret;
    }
    match arg.typ.as_str() {
        "int" => "i32".into(),
        "uint" => "u32".into(),
        "fd" => "RawFd".into(),
        "string" => "Array /*placeholder for string*/".into(),
        "fixed" => "Fixed".into(),
        "array" => "Array".into(),
        "new_id" => {
            let base = if let Some(ref interface) = arg.interface {
                format!("interfaces::{}", wl_to_rust_name(&interface[3..]))
            }
            else {
                "Interface".into()
            };
            
            format!("New<{}>", base)
        }
        "object" => {
            if let Some(ref interface) = arg.interface {
                format!("interfaces::{}", wl_to_rust_name(&interface[3..]))
            }
            else {
                "Interface".into()
            }
        }
        _ => panic!("Unkown type: {:?}", arg)
    }
}

fn doc_comment(out: &mut File, summary: &str, description: &str) {
    let lineend = "\n///";
    
    write!(out, "{} {}", lineend, summary).unwrap();
    write!(out, "{0}{0} ", lineend).unwrap();
    write!(out, "{}\n", description.replace('\n', lineend)).unwrap();    
}

fn write_interface_structs_and_enums(out: &mut File, interfaces: &[Interface]) {
    out.write(b"pub mod interfaces {\n\
                use nix::Error as NixError;\n\
                \n\
                use std::os::unix::io::RawFd;\n\
                use ::{Array, Interface, Fixed, New, interfaces};\n\
                use nix::sys::socket::{send, sendmsg, MsgFlags, ControlMessage};\n\
                use nix::sys::uio::IoVec;").unwrap();
    
    for interface in interfaces {
        doc_comment(out, &interface.summary, &interface.description);
        let name = wl_to_rust_name( &interface.name[3..] );
        let lname = &interface.name[3..];
        write!(out, "pub mod {3} {{\n\
                        #![allow(non_upper_case_globals, non_camel_case_types, unused_imports)]\n\
                        use std::os::unix::io::RawFd;\n\
                        use {{Array, Fixed, New, Interface, interfaces}};\n\
                        use super::*;\n\n\
                        #[derive(Debug,Clone,Copy,PartialEq,Eq)]\n\
                        /// See module documentation\n\
                        pub struct {0}(pub u32);\n
                        \n\
                        impl {0} {{\n\
                            pub fn interface_name() -> Array {{\n\
                                Array::from_string(\"{1}\".into())\n\
                            }}\n\
                            pub fn interface_version() -> u32 {{\n\
                                {2}\n\
                            }}\n\
                            pub fn id(self) -> u32 {{\n\
                                self.0\n\
                            }}\n\
                            pub fn set_id(&mut self, id: u32) {{\n\
                                self.0 = id\n\
                            }}\n\
                        }}\n", name, interface.name, interface.version, lname).unwrap();
        write_enums(out, interface);
        write_request_structs(out, interface);
        write_event_structs(out, interface);
        write!(out, "}}\n\
                     pub use self::{}::{};\n",
                     lname, name).unwrap();
    }
    
    write_event_methods(out, interfaces);
    write_request_methods(out, interfaces);
                
    out.write(b"}\n").unwrap();
}

fn write_interfaces_enum(out: &mut File, interfaces: &[Interface]) {
    let header = b"/// Union type of interfaces\n\
                  #[derive(Debug, Clone, Copy,PartialEq,Eq)]\n\
                  pub enum Interface {\n";
    out.write(header).unwrap();
    
    // Write enum
    for interface in interfaces {
        // TODO: Should I actually comment this and the struct?
        doc_comment(out, &interface.summary, &interface.description);
        
        let name = wl_to_rust_name( &interface.name[3..] );
        write!(out, "{0}({0}),\n", name).unwrap();
    }
    out.write(b"}\n").unwrap();
    
    // Write get name function
    out.write(b"impl Interface {\n\
                    pub fn interface_name(self) -> Array {\n\
                        match self {\n").unwrap();
    for interface in interfaces {
        let name = wl_to_rust_name( &interface.name[3..] );
        write!(out, "Interface::{0}(_) => {0}::interface_name(),\n", name).unwrap();
    }
    
    // Write from name function
    out.write(b"    }\n\
                }\n\
                pub fn from_name(name: &str, id: u32) -> Option<Interface> {\n\
                    match name {\n").unwrap();
    for interface in interfaces {
        let rname = wl_to_rust_name( &interface.name[3..] );
        write!(out, "\"{0}\" => Some(Interface::{1}({1}(id))),\n",
                     interface.name,
                     rname).unwrap();
    }
    out.write(b"_ => None\n").unwrap();
    
    // Write id function
    out.write(b"    }\n\
                }\n\
                pub fn id(self) -> u32 {\n\
                    match self {\n").unwrap();
    for interface in interfaces {
        let rname = wl_to_rust_name( &interface.name[3..] );
        write!(out, "Interface::{0}({0}(id)) => id,\n",
                     rname).unwrap();
    }
    
    // Write set id function
    out.write(b"    }\n\
                }\n\
                pub fn set_id(&mut self, id: u32) {\n\
                    match *self {\n").unwrap();
    for interface in interfaces {
        let rname = wl_to_rust_name( &interface.name[3..] );
        write!(out, "Interface::{0}(ref mut inner) => inner.set_id(id),\n",
                     rname).unwrap();
    }
    
    // Write get version function
    out.write(b"    }\n\
                }\n\
                pub fn interface_version(self) -> u32 {\n\
                    match self {\n").unwrap();
    for interface in interfaces {
        let name = wl_to_rust_name( &interface.name[3..] );
        write!(out, "Interface::{0}(_) => {0}::interface_version(),\n", name).unwrap();
    }
    out.write(b"        }\n\
                    }\n\
                }\n").unwrap();
                
    // Write From traits impls
    for interface in interfaces {
        let name = wl_to_rust_name( &interface.name[3..] );
        write!(out, "impl From<{0}> for Interface {{\n\
                         fn from(x: {0}) -> Interface {{\n\
                             Interface::{0}(x)\n\
                         }}\n\
                     }}\n",
                     name).unwrap();
    }
}

fn write_request_uses(out: &mut File, interfaces: &[Interface]) {
    out.write(b"pub mod requests {\n").unwrap();
    
    for interface in interfaces {
        let iname = wl_to_rust_name(&interface.name[3..]);
        let lname = &interface.name[3..];
        for request in &interface.requests {
            let rname = wl_to_rust_name(&request.name);
            write!(out, "pub use interfaces::{0}::{2} as {1}{2};\n", lname, iname, rname).unwrap();
        }
    }
    
    out.write(b"}\n").unwrap();
}

fn write_request_enum(out: &mut File, interfaces: &[Interface]) {
    out.write(b"\
        /// Enumeration of all possible request types.\n\
        #[derive(Debug)]
        pub enum Request {\n").unwrap();
    for interface in interfaces {
        let iname = wl_to_rust_name(&interface.name[3..]);
        let lname = &interface.name[3..];
        for request in &interface.requests {
            // TODO: Should I put documentation here as well?
            let rname = wl_to_rust_name(&request.name);
            write!(out, "{0}{1}({0}, {2}::{1}),\n", iname, rname, lname).unwrap();
        }
    }
    
    out.write(b"}\n").unwrap();
}

fn write_event_enum(out: &mut File, interfaces: &[Interface]) {
    out.write(b"\
        /// Enumeration of all possible request types.\n\
        #[derive(Debug)]
        pub enum Event {\n").unwrap();
    for interface in interfaces {
        let iname = wl_to_rust_name(&interface.name[3..]);
        let lname = &interface.name[3..];
        for event in &interface.events {
            // TODO: Should I put documentation here as well?
            let rname = wl_to_rust_name(&event.name);
            write!(out, "{0}{1}({0}, {2}::{1}),\n", iname, rname, lname).unwrap();
        }
    }
    
    out.write(b"}\n").unwrap();
}

fn write_array_arg(out: &mut File, name: &str) {
    write!(out, "let len = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as usize;\n\
                 data = &data[1..];\n\
                 if data.len() * 4 < len {{\n\
                     throw_new!(ReadError::Malformed);\n\
                 }}\n\
                 let {} = Array {{\n\
                     contents: std::slice::from_raw_parts(data.as_ptr() as *const u8, len as usize).to_vec()\n\
                 }};\n\
                 data = &data[len/4 ..];\n\
                 if len % 4 != 0 {{\n\
                     data = &data[1..];\n\
                 }};\n\n", name).unwrap();
}

fn write_uint_arg(out: &mut File, name: &str) {
    write!(out, "let {} = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));\n\
                data = &data[1..];\n\n", name).unwrap();
}

// The meat of the argument parsing is in this function
fn write_message_parse_args(out: &mut File, interface: &Interface, msg: &Message) {
    for arg in &msg.args {
        match arg.typ.as_str() {
            "new_id" => {
                // Sidenote: This feels like a ridiculous way to
                // describe the spec in xml... anyways.
                match arg.interface {
                    Some(ref ret_interface) => {
                        let ret_interface = wl_to_rust_name(&ret_interface[3..]);
                        write!(out, "let {0}_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));\n\
                            let {0} = {1}({0}_id_rust);\n\
                            let {0} = New({0});\n\
                            data = &data[1..];\n\n",
                            arg.name,
                            ret_interface).unwrap();
                    },
                    None => {
                        let ret_interface = arg.interface_arg.as_ref().unwrap();
                        write!(out, "let {0}_id_rust = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete)));\n\
                        let {0} = {{\n\
                            let name_rust = up!({1}.as_str().ok_or(throw::Error::new(ReadError::Malformed)));
                            Interface::from_name(name_rust, {0}_id_rust)\n\
                        }};\n\
                        let {0} = up!({0}.ok_or(throw::Error::new(ReadError::Malformed)));\n\
                        let {0} = New({0});\n\
                        data = &data[1..];\n\n",
                        arg.name,
                        ret_interface).unwrap();
                        
                    }
                }
                if let Some( ref ret_interface ) = arg.interface {
                    wl_to_rust_name(&ret_interface[3..]) 
                }
                else {
                    "ClientCreated".into()
                };

            },
            "object" => {
            let ret_interface = 
                if let Some( ref ret_interface ) = arg.interface {
                    wl_to_rust_name(&ret_interface[3..]) 
                }
                else {
                    panic!("Expected interface for object type")
                };
            write!(out, "let {0} = {1}(*up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))));\n\
                            data = &data[1..];\n\n", arg.name, ret_interface).unwrap();
            }
            "int" if arg.enum_ == None => {
                write!(out, "let {} = *up!(data.get(0).ok_or(throw::Error::new(ReadError::Incomplete))) as i32;\n\
                            data = &data[1..];\n\n", arg.name).unwrap();
            },
            "uint" if arg.enum_ == None => {
                write!(out, "// not an emum\n").unwrap();
                write_uint_arg(out, &arg.name);
            },
            "uint" | "int" => {
                // With enum
                write!(out, "// enum!\n").unwrap();
                write_uint_arg(out, &arg.name);
                let enum_ = arg.enum_.as_ref().unwrap();
                write!(out, "let {0} = ::interfaces::{1}::{2}({0});\n\n",
                    arg.name,
                    &interface.name[3..],
                    enum_).unwrap();
            }
            "array" | "string" => {
                write_array_arg(out, &arg.name);
            },
            "fd" => {
                write!(out, "let {} = fds[0];\n\
                            fds = &fds[1..];\n\n", &arg.name).unwrap();
            },
            _ => write!(out, "let {} = unimplemented!(); /*{}*/", &arg.name, &arg.typ).unwrap(),
        }
    } 
}

// This and write_connection_get_event are effectively the same, just replace request with event.
// Unfortunately it is hard to impossible to factor this out.
fn write_connection_get_request(out: &mut File, interfaces: &[Interface]) {
    out.write(b"unsafe fn get_request<T: ObjectStore>(objects: &T, header: Header, mut data: &[u32], mut fds: &[fd]) -> Result<Request, throw::Error<ReadError>> {\n\
                    // TODO: Gracefully handle broken clients\n\
                    let object = up!(objects.get(header.id)\n\
                        .ok_or(throw::Error::new(ReadError::NoSuchObject)));\n\
                    match (object, header.opcode) {").unwrap();
     
     for interface in interfaces {
         let iname = wl_to_rust_name(&interface.name[3..]);
         let lname = &interface.name[3..];
         for (i, request) in interface.requests.iter().enumerate() {
             let rname = wl_to_rust_name(&request.name);
             
             if request.args.len() == 0 {
                 let rname = wl_to_rust_name(&request.name);
                 write!(out, "(Interface::{0}(object), {1}) => \
                               Ok(Request::{0}{2}(object, {3}::{2})),", iname, i, rname, lname).unwrap();
                 continue;
             }
             
             write!(out, "(Interface::{}(object), {}) => {{", iname, i).unwrap();
            
             write_message_parse_args(out, interface, request);
             
             write!(out, "Ok(Request::{0}{1}(object, {2}::{1} {{", iname, rname, lname).unwrap();
             for (i, arg) in request.args.iter().enumerate() {
                 write!(out, "{0}: {0}", arg.name).unwrap();
                 if i + 1 < request.args.len() {
                     out.write(b",").unwrap();
                 }
             }
             out.write(b"}))\n},\n").unwrap();
         }
     }
                
     out.write(b" _ => throw_new!(ReadError::UnkownMethod(object, header.opcode))\n\
                }\n}\n\n").unwrap();
}

fn write_connection_get_event(out: &mut File, interfaces: &[Interface]) {
    out.write(b"unsafe fn get_event<T: ObjectStore>(objects: &T, header: Header, mut data: &[u32], mut fds: &[fd]) -> Result<Event, throw::Error<ReadError>> {\n\
                    // TODO: Gracefully handle broken clients\n\
                    let object = up!(objects.get(header.id)\n\
                        .ok_or(throw::Error::new(ReadError::NoSuchObject)));\n\
                    match (object, header.opcode) {").unwrap();
     
     for interface in interfaces {
         let iname = wl_to_rust_name(&interface.name[3..]);
         let lname = &interface.name[3..];
         for (i, event) in interface.events.iter().enumerate() {
             let rname = wl_to_rust_name(&event.name);
             
             if event.args.len() == 0 {
                 let rname = wl_to_rust_name(&event.name);
                 write!(out, "(Interface::{0}(object), {1}) => \
                               Ok(Event::{0}{2}(object, {3}::{2})),", iname, i, rname, lname).unwrap();
                 continue;
             }
             
             write!(out, "(Interface::{}(object), {}) => {{", iname, i).unwrap();
            
             write_message_parse_args(out, interface, event);
             
             write!(out, "Ok(Event::{0}{1}(object, {2}::{1} {{", iname, rname, lname).unwrap();
             for (i, arg) in event.args.iter().enumerate() {
                 write!(out, "{0}: {0}", arg.name).unwrap();
                 if i + 1 < event.args.len() {
                     out.write(b",").unwrap();
                 }
             }
             out.write(b"}))\n},\n").unwrap();
         }
     }
                
     out.write(b" _ => throw_new!(ReadError::UnkownMethod(object, header.opcode))\n\
                }\n}\n\n").unwrap();
}

fn sanitize_name(name: &str) -> &str {
    match name {
        "move" => "move_",
        _ => name,
    }
}

fn write_message_method(out: &mut File, interface: &Interface, msg: &Message, opcode: usize) {
    // Method signature
    
    write!(out, "pub fn {}(self, con_fd: RawFd", 
            sanitize_name(&msg.name)).unwrap();
    for arg in &msg.args {
        let typ = wl_to_rust_type(interface, arg);
        write!(out, ",{}: {}", arg.name, typ).unwrap();
    }
    out.write(b") -> Result<(), NixError> {\n").unwrap();
    
    // Method body
    
    // Buffer generation (differs for fixed and variable width)
    if msg.args.iter().any(|x| x.typ == "array" || x.typ == "string") {
        // Variable width
        let minimum = 2 + msg.args.iter().filter(|x| x.typ != "fd").count();
        write!(out, "let mut len = {};\n", minimum).unwrap();
        
        for var_arg in msg.args.iter().filter(|x| x.typ == "array" || x.typ == "string") {
            write!(out, "len += if {0}.contents.len() % 4 == 0 {{\n\
                                {0}.contents.len() / 4\n\
                            }}\n\
                            else {{\n\
                                1 + ({0}.contents.len() / 4)\n\
                            }};\n", var_arg.name).unwrap();
        }
        
        // Surely there is a better way, I don't particularly care if the vec
        // is initialized or not.
        out.write(b"let mut vec_buf = Vec::with_capacity(len);\n\
                    unsafe{ vec_buf.set_len( len ) };\n\
                    let mut buf: Box<[u32]> = vec_buf.into_boxed_slice();\n\n").unwrap();
                        
    }
    else {
        // Fixed width, note that all fixed width args are 32 bits
        let len = 2 + msg.args.iter().filter(|x| x.typ != "fd").count(); 
        write!(out, "let len = {0};\n\
                        let mut buf: [u32; {0}] = unsafe{{ ::std::mem::uninitialized() }};\n\n",
                        len).unwrap();
    }
    
    // Write Header
    write!(out, "assert!( len * 4 < ::std::u16::MAX as usize );
                    buf[0] = self.0;\n\
                    buf[1] = (((len * 4) as u32) << 16) | {} as u32;\n\n",
                    opcode).unwrap();
    
    // Fill in types
    let mut fd_used = false;
    out.write(b"let mut ind = 2;\n\n").unwrap();
    for arg in &msg.args {
        match arg.typ.as_str() {
            "int" | "uint" if arg.enum_ == None => {
                write!(out, "buf[ind] = {} as u32;\n\
                                ind += 1;\n\n", 
                                arg.name).unwrap();
            },
            "int" | "uint" => {
                // Enum type
                write!(out, "buf[ind] = {}.0 as u32;\n\
                                ind += 1;\n\n", 
                                arg.name).unwrap();
            },
            "fixed" => {
                write!(out, "buf[ind] = {}.0;\n\
                                ind += 1;\n\n",
                                arg.name).unwrap();
            },
            "object" => {
                write!(out, "buf[ind] = {}.id();\n\
                                ind += 1;\n\n",
                                arg.name).unwrap();
            },
            "new_id" => {
                write!(out, "buf[ind] = ({}.0).id();\n\
                                ind += 1;\n\n",
                                arg.name).unwrap();
            },
            "string" | "array" => {
                write!(out, "buf[ind] = {0}.contents.len() as u32;\n\
                                unsafe {{\n\
                                    let dst_ptr = buf[ind + 1..].as_mut_ptr() as *mut u8;\n\
                                    let src_ptr = {0}.contents.as_ptr();\n\
                                    ::std::ptr::copy_nonoverlapping(src_ptr, dst_ptr, {0}.contents.len());\n\
                                }};\n\
                                if {0}.contents.len() % 4 == 0 {{\n\
                                    ind += 1 + ({0}.contents.len() / 4);\n\
                                }}\n\
                                else {{\n\
                                    ind += 2 + ({0}.contents.len() / 4);\n\
                                }}\n\n", arg.name).unwrap();
                                
            },
            "fd" => {
                // Currently we only support one fd per message (which is all the protocol uses)
                assert!(!fd_used);
                fd_used = true;
                
                write!(out, "let fds = [{}];\n", arg.name).unwrap();
            },
            _ => panic!("Unkown type: {}", arg.typ),
        }
    }
    
    // Send the message
    
    /* mio code for the fd case
        let iov = [nix::IoVec::from_slice(buf)];
        let fds = [fd];
        let cmsg = nix::ControlMessage::ScmRights(&fds);
        nix::sendmsg(self.io.as_raw_fd(),&iov, &[cmsg], 0, None)
    */
    
    out.write(b"let buf_8 = unsafe{ \n\
                    ::std::slice::from_raw_parts( buf.as_ptr() as *const u8, buf.len() * 4 )\n\
                };\n").unwrap();
    if fd_used {
        out.write(b"let iov = [IoVec::from_slice(buf_8)];\n\
                    let cmsg = ControlMessage::ScmRights(&fds);\n\
                    sendmsg(con_fd, &iov, &[cmsg], MsgFlags::empty(), None).map(|x| ())\n"
                    ).unwrap();
    }
    else {
        out.write(b"send(con_fd, buf_8, MsgFlags::empty()).map(|x| ())\n")
                    .unwrap();
    }
    out.write(b"}\n").unwrap();
}

fn write_event_methods(out: &mut File, interfaces: &[Interface]) {
    for interface in interfaces {
        let iname = wl_to_rust_name(&interface.name[3..]);
        write!(out, "#[allow(unused_mut, unused_variables)]\n\
                     impl {} {{\n", iname).unwrap();
        
        for (opcode, event) in interface.events.iter().enumerate() {
            doc_comment(out, &format!("Event: {}", &event.summary), &event.description);
            write_message_method(out, interface, event, opcode);
        }
            
        
        out.write(b"}").unwrap();
    }
}

fn write_request_methods(out: &mut File, interfaces: &[Interface]) {
    for interface in interfaces {
        let iname = wl_to_rust_name(&interface.name[3..]);
        write!(out, "#[allow(unused_mut, unused_variables)]\n\
                     impl {} {{\n", iname).unwrap();
        
        for (opcode, request) in interface.requests.iter().enumerate() {
            doc_comment(out, &format!("Request: {}", &request.summary), &request.description);
            write_message_method(out, interface, request, opcode);
        }
            
        
        out.write(b"}").unwrap();
    }
}

fn write_enums(out: &mut File, interface: &Interface) {
    for enum_ in &interface.enums {
        // TODO: Make doc comments work with cenum!
        //doc_comment(&enum_.summary, &enum_.description).unwrap();
        write!(out, "cenum!{{ {} u32 {{\n", enum_.name).unwrap();
        println!("enum: {}", enum_.name);
        println!("cenum!{{ {} u32 {{", enum_.name);
        
        for entry in &enum_.entrys {
            println!("\tentry: {}", entry.name);
            if let Some(ref summary) = entry.summary {
                // TODO: Make doc comments work with cenum!
                write!(out, "// {}\n", summary).unwrap();
            }
            write!(out, "{} = {},\n", wl_to_rust_name(&entry.name), entry.value).unwrap();
        }
        
        out.write(b"}}\n").unwrap();
        
        if enum_.bitfield {
            // TODO: Where is the bitnot operator?
            write!(out,
                "impl ::std::ops::BitAnd for {0} {{
                    type Output = {0};
                    fn bitand(self, rhs: {0}) -> {0} {{
                        {0}(self.0 & rhs.0)
                    }}
                }}
                impl ::std::ops::BitOr for {0} {{
                    type Output = {0};
                    fn bitor(self, rhs: {0}) -> {0} {{
                        {0}(self.0 | rhs.0)
                    }}
                }}
                impl ::std::ops::BitXor for {0} {{
                    type Output = {0};
                    fn bitxor(self, rhs: {0}) -> {0} {{
                        {0}(self.0 ^ rhs.0)
                    }}
                }}", enum_.name).unwrap();
        }
    }
}

fn write_message_struct(out: &mut File, interface: &Interface, msg: &Message) {
    let rname = wl_to_rust_name(&msg.name);
    
    if msg.args.len() > 0 {
        write!(out, "#[derive(Debug)]\n\
                        pub struct {} {{\n", rname).unwrap();
        
        for arg in &msg.args {
            if let Some(ref summary) = arg.summary {
                write!(out, "/// {}\n", summary).unwrap();
            }
            let typ = wl_to_rust_type(interface, &arg);
            
            write!(out, "pub {}: {},\n", arg.name, typ).unwrap();
            // TODO: Option for allow_null
            // TODO: handle enum_ types somehow
            // TODO: Handle Interface for new_id.
        }
        
        write!(out, "}}\n\n").unwrap();
    }
    else {
        write!(out, "#[derive(Debug)]\n\
                        pub struct {};\n\n", rname).unwrap();
    }
}

fn write_request_structs(out: &mut File, interface: &Interface) {
    for request in &interface.requests {
        doc_comment(out, &format!("Request: {}", &request.summary), &request.description);
        write_message_struct(out, interface, request);
    }
}

fn write_event_structs(out: &mut File, interface: &Interface) {
    for event in &interface.events {
        doc_comment(out, &format!("Event: {}", &event.summary), &event.description);
        write_message_struct(out, interface, event);
    }
}

static HEADER: &'static str = stringify!{
    #![feature(question_mark)]
    #![allow(unused_assignments)]
    extern crate nix;
    
    pub use nix::Error as NixError;
    
    #[macro_use]
    extern crate throw;
    
    #[macro_use]
    extern crate cenum;
    
    use std::os::unix::io::RawFd as fd;
    
    use interfaces::*;
    use requests::*;
    
    // Public Interfaces
};


pub fn write_message_iter(out: &mut File, name: &str, item: &str, func: &str) {
    write!(out, "\n\
        impl<T: ObjectStore> Connection<T> {{\n\
            pub fn get_{2}s(&mut self) -> Result<{0}, GetMessagesError> {{\n\
                use nix::sys::uio::IoVec;\n\
                use nix::sys::socket::{{recvmsg, CmsgSpace, MsgFlags, ControlMessage}};\n\
                \n\
                let mut ret_fds = vec!();\n\
                \n\
                \n\
                // Borrowed from mio::sys::unix::uds;\n\
                // (Then altered)\n\
                \n\
                // Buf shenanigans needed for alignment!\n\
                let mut buf_32: [u32; 1024] = unsafe{{ std::mem::uninitialized() }};\n\
                let len = {{\n\
                    let buf_8: &mut [u8; 4096] = unsafe{{ std::mem::transmute(&mut buf_32) }};\n\
                    let buf: &mut [u8] = &mut *buf_8;\n\
                \n\
                    \n\
                    let iov = [IoVec::from_mut_slice(buf)];\n\
                    let mut cmsgspace: CmsgSpace<[fd; 1024]> = CmsgSpace::new();\n\
                    let msg = try!(recvmsg(self.fd, &iov, Some(&mut cmsgspace), MsgFlags::empty())\n\
                                .map_err(|x| GetMessagesError::Nix(x)));\n\
                    for cmsg in msg.cmsgs() {{\n\
                        if let ControlMessage::ScmRights(fds) = cmsg {{\n\
                            ret_fds = fds.to_vec();\n\
                        }}\n\
                    }}\n\
                    \n\
                    if msg.bytes == 0 {{\n\
                        return Err(GetMessagesError::Closed);\n\
                    }}\n\
                    \n\
                    assert!(msg.bytes % 4 == 0);\n\
                    msg.bytes / 4\n\
                }};\n\
                \n\
                let data = buf_32[..len].to_vec();\n\
                \n\
                // Debugging\n\
                print!(\"Recieved[ \");\n\
                for i in &data {{\n\
                    print!(\"{{:0>8x}} \", i);\n\
                }}\n\
                println!(\"]\");\n\
                \n\
                Ok({0} {{\n\
                    data: data,\n\
                    fds: ret_fds,\n\
                    start: 0\n\
                }})\n\
            }}\n\
        }}\n\
        /// Iterator over requests in read data\n\
        pub struct {0} {{\n\
            data: Vec<u32>,\n\
            fds: Vec<fd>,\n\
            start: usize,\n\
        }}\n\
        \n\
        impl {0} {{\n\
            pub fn next<T: ObjectStore>(&mut self, objects: &T) -> Option<Result<{1},throw::Error<ReadError>>> {{\n\
                if self.start == self.data.len() {{\n\
                    return None;\n\
                }}\n\
                \n\
                if self.data.len() < self.start + 2 {{\n\
                    return Some(err!(ReadError::Incomplete))\n\
                }}\n\
                \n\
                let data = &self.data[self.start ..];\n\
                let header = unsafe{{ *(data.as_ptr() as *const Header) }};\n\
                \n\
                if header.size % 4 != 0 {{\n\
                    return Some(err!(ReadError::Malformed));\n\
                }}\n\
                \n\
                let len = header.size as usize / 4;\n\
                \n\
                if self.start + len > self.data.len() {{\n\
                    return Some(err!(ReadError::Incomplete));\n\
                }}\n\
                \n\
                // Debugging\n\
                print!(\"data[ \");\n\
                for x in &data[.. len] {{\n\
                    print!(\"{{:x}} \", x);\n\
                }}\n\
                println!(\"]\");\n\
                \n\
                \n\
                self.start += len;\n\
                Some(unsafe{{get_{2}(objects, header, &data[2.. len], &self.fds) }})\n\
            }}\n\
        }}\n", name, item, func).unwrap();
}

static DATA_TYPES: &'static str = stringify!{
    /// Fixed width decimal
    /// Wire format: 23 bits integer, 8 bits decimal
    /// Rust format is identical
    #[repr(C)]
    #[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
    pub struct Fixed(u32);

    /// Wayland Array, also used for string
    /// Wire format: 32 bit length followed by data. If string null terminated string
    /// Rust format differs, strings do not include null terminator.
    #[derive(Debug)]
    pub struct Array {
        pub contents: Vec<u8>
    }
    
    impl Array {
        pub fn from_string(s: String) -> Array {
            let mut ret = s.into_bytes();
            
            // Because of course we need both null termination and a length! /s
            ret.push(0);
            
            Array{ contents: ret }
        }
        
        pub fn as_str(&self) -> Option<&str> {
            std::str::from_utf8(&self.contents[.. self.contents.len() - 1]).ok()
        }
    }

    /// New ID
    ///
    /// Wrapper type indicating the contained item is a new id/object to allow the app
    /// to initialized datastructures before use.
    #[derive(Debug, PartialEq, Eq)]
    pub struct New<T>(pub T);
};

static CONNECTION: &'static str = stringify!{
    pub trait ObjectStore: Sized + Default {
        fn get(&self, id: u32) -> Option<Interface>;
    }
    
    pub struct Connection<T: ObjectStore> {
        pub fd: fd,
        pub objects: T,
    }

    pub enum GetMessagesError {
        Nix(NixError),
        Closed,
    }

    impl<T: ObjectStore> Connection<T> {
        pub fn new(fd: fd) -> Connection<T> {
            Connection {
                fd: fd,
                objects: T::default(),
                // TODO remove, vec!(None, Some(Interface::Display(interfaces::Display(1)))),
            }
        }
    }
    
    
    /// Please note that error types are given on a best effort bases,
    /// a malformed request may show as incomplete.
    #[derive(Debug, Copy, Clone, PartialEq, Eq)]
    pub enum ReadError {
        Incomplete,
        Malformed,
        NoSuchObject,
        UnkownMethod(Interface, u16)
    }
};

static PRIVATE: &'static str = stringify!{
    // Private implementation

    /// Header sent for each request    
    #[repr(C)]
    #[derive(Debug, Clone, Copy)]
    struct Header {
        id: u32,
        opcode: u16,
        size: u16
    }
};