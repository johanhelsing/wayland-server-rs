A pure rust implementation of the wayland protocol, theoretically works with both servers and clients right now, I plan to seperate those somehow in the future (either cfg flags or different libraries).

I'm not sure I'm handling errors correctly, do they follow a bind like interface or an object like interface for newid?

I'm not handling versioning at all right now.

## Building

`build.rs` contains all the handwritten code, `src/lib.rs` is built by compiling and running `build.rs`. The easiest way to do this the first time is to uncomment the build line in `Cargo.toml`, on future runs `rustc build.rs -L target/debug/deps && build`.
